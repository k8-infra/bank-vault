kubectl create namespace argocd
helm repo add argo https://argoproj.github.io/argo-helm
helm repo update
export ARGOCD_EXEC_TIMEOUT=300
helm install gitops argo/argo-cd -n argocd -f argo-values.yaml

NAME=$(kubectl get pods -n argocd -l app.kubernetes.io/name=argocd-server -o name | cut -d'/' -f 2)
echo "waiting for pod $NAME"

while [[ $(kubectl get pods $NAME -n argocd -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}') != "True" ]]; 
  do 
     sleep 60;
  done

kubectl port-forward service/gitops-argocd-server -n argocd 8080:80 &